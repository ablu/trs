# based on ledge-secure-qemuarm64
MACHINEOVERRIDES =. "qemuarm64:trs-qemuarm64:ledge-secure-qemuarm64:"

# based on basic poky qemu machine
require ${COREBASE}/meta/conf/machine/qemuarm64.conf

# use poky default qemuarm64 DEFAULTTUNE, don't modify via ewaol
EWAOL_GENERIC_ARM64_FILESYSTEM = "0"

KMACHINE = "qemuarm64"

UBOOT_MACHINE = "qemu_arm64_defconfig"

QB_MACHINE = "-machine virt,secure=on"
QB_OPT_APPEND += "-no-acpi"
QB_MEM = "-m 2048"
QB_DEFAULT_FSTYPE = "wic"
QB_DEFAULT_BIOS = "flash.bin"
QB_FSINFO = "wic:no-kernel-in-fs"
QB_ROOTFS_OPT = ""
QB_CPU = "-cpu cortex-a57"

# auto detection not working for testimage:
QEMU_USE_SLIRP = "1"

# only localhost to access via ssh
QB_SLIRP_OPT = "-netdev user,id=net0,hostfwd=tcp:127.0.0.1:2222-:22"

TEST_SERVER_IP = "127.0.0.1"

# Due to rootfs encryption, boot to serial login prompt may
# take really long on a loaded machine
TEST_QEMUBOOT_TIMEOUT = "2000"

# set this correclty for testimage.bbclass, will be wrong for
# testexport.bbclass but that can be overwritten using command line
# argument:
# ./oe-test runtime --target-type "simpleremote"
TEST_TARGET = "qemu"

# kernel is in the image, should not be loaded separately
QB_DEFAULT_KERNEL = "none"

# setup SW based TPM for testing, note socket file path has 107 character
# length limitations from sockaddr_un
QB_SETUP_CMD = " \
   set -ex; pwd; which swtpm; swtpm --version; which swtpm_setup; \
   test -d '${IMAGE_BASENAME}_swtpm' || ( mkdir -p '${IMAGE_BASENAME}_swtpm' && \
       swtpm_setup --reconfigure --tpmstate '${IMAGE_BASENAME}_swtpm' --tpm2 --pcr-banks sha256 --config $(dirname $( which swtpm ) )/../../etc/swtpm_setup.conf ) && \
   test -f '${IMAGE_BASENAME}_swtpm/tpm2-00.permall' && \
   swtpm socket --tpmstate dir='${IMAGE_BASENAME}_swtpm' \
         --ctrl type=unixio,path='${IMAGE_BASENAME}_swtpm/swtpm-sock' \
         --flags startup-clear \
         --log level=30 --tpm2 -t -d \
"

QB_OPT_APPEND += "-chardev socket,id=chrtpm,path='${IMAGE_BASENAME}_swtpm/swtpm-sock' -tpmdev emulator,id=tpm0,chardev=chrtpm -device tpm-tis-device,tpmdev=tpm0"

KERNEL_IMAGETYPE = "Image"
PREFERRED_PROVIDER_virtual/kernel = "linux-yocto"

INITRAMFS_IMAGE = "ledge-initramfs"
WKS_FILE_DEPENDS += "ledge-initramfs"
do_image_wic[depends] += "ledge-initramfs:do_image_complete"
WKS_FILE = "ledge-secure.wks.in"
IMAGE_BOOT_FILES = "${KERNEL_IMAGETYPE}"

MACHINE_FEATURES:append = " optee-ftpm"
MACHINE_FEATURES:append = " optee"
MACHINE_FEATURES:append = " tpm2"
MACHINE_FEATURES:append = " grub"
