About TRS
#########
Developing software on its own is complicated and requires time, skills and lots
of efforts. But being good at writing individual software isn't sufficient in
this day and age. Systems are inherently complicated with lots of components
interacting with each other. We have to deal with intra communication as well as
external communication with remote systems. All aspects of security has to be
considered, standards needs to be addressed and systems needs to be tested not
only as individual components, but as coherent systems. For device manufacturers
this becomes a real challenge, which is very costly both in terms on time and
effort.

As an answer to the challenges presented, Linaro have created **TRS (Trusted
Reference Stack)**, which is an umbrella project and a software stack containing
well tested software components making up a solid base for efficient development
and for building unique and differentiating end-to-end use cases.

.. _about_goals:

Goals and key properties
************************
* Common platform for deliverables from Linaro.
* Include all Linaro test suites and test frameworks making CI/CD and regression
  testing fast, valuable and efficient.
* Efficient development environment for engineers.
* A product ready reference implementation.
* Configurable to be able to meet different needs.
* Common ground and building block for Blueprints and similar targets.
* Interoperability making it possible to use alternative implementations.
* Pre-silicon IP support in environments like QEMU etc.

Firmware Software Components
****************************
The firmware components for TRS is provided by :ref:`Trusted Substrate <Firmware>`
more details, please look :ref:`here <Software Components>`

Releases
********

v0.2 - 2023-03-07
~~~~~~~~~~~~~~~~~

  * Stable CI
     * xtest from OP-TEE (nightly and merge request)
     * Measured boot tests (nightly and merge request)
     * Secure boot (nightly and merge request)
     * ACS 1.0 manually, except QEMU, where it is in CI.
  * Platform support, meaning that they work with TRS
     * QEMU
     * RockPi4
     * Synquacer
  * New features
     * Authenticated policies.
     * Grub as part of the boot flow.

v0.1 - 2022-12-16
~~~~~~~~~~~~~~~~~

* Restructured the layer structure, by moving some layers up to the top level.
* QEMU is built by Yocto instead of relying on the host installed QEMU version.
* Changed repo release/branching strategy.
* Trusted Substrate documentation has moved into a subsection of TRS.
* Uses Trusted Substrate v0.2.
* Uses LEDGE Secure v0.1.
* Features enabled: LUKS disc encryption, Measured Boot, UEFI Secure Boot using
  U-boot.

v0.1-beta - 2022-09-02
~~~~~~~~~~~~~~~~~~~~~~
.. note::

    This release is slightly flawed, mostly due to the fact that code was
    checked out when the build was started and the code did not always track
    stable commits.

* Builds TRS for the QEMU target.
* Boot cleanly up to the login prompt.
* Nothing tested.
* RockPi4 works, but not officially part of the v0.1-beta release.
