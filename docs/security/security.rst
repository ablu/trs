Threat models
#############
We're leveraging the `MITRE D3FEND threat model matrix`_ as a basis for the
threat modeling work in the TRS. Although MITRE D3FEND is more aimed at regular
PC use, we believe it is a good and comprehensive summary of potential attacks
to a lot of use cases in TRS. MITRE D3FEND covers the generic type of threats.
In addition to that we will also identify the specific threats based on the
assets that we're trying to protect. Re-use is key here, the first use-cases
that we implement will cover quite a bit of mitigation techniques. For new use
cases we anticipate that these should be able to leverage mitigations already
implemented for other use cases.

Use cases
*********

1. Attested containers
======================

Assets
------
.. list-table:: Assets in attested containers
    :widths: 50 50
    :header-rows: 1
    :align: left

    * - Asset
      - Description

    * - Private key(s) used to sign the container images.
      - Private keys will be used to sign the container images.

    * - Public key(s) used to verify signature.
      - Although not secret, they must be immutable in the system.

    * - PCR registers in the TPM
      - They tell the true and expected state of a system. 

    * - Audit log files
      - Files under ``/var/...`` tracking events in the form of audit logs.

    * - Authentication Tokens
      - When leveraging backends, it's common to get an authorization token from
        the backend provider.

    * - Environment variables
      - Tokens and passwords sometimes needs to be stored in environment
        variables.

    * - Kernel command line
      - Information provided via Linux kernel commandline could be vital (for
        the security of the system).

    * - U-Boot commandline
      - Should be locked down on a production system to avoid system
        modification.



Hardening
---------
.. list-table:: Threat model attested containers
    :widths: 33 33 34
    :header-rows: 1
    :align: left

    * - Threat
      - Description
      - Mitigation

    * - **Insecure configuration** (`D3-ACH`_)
      - Software sometimes comes with default configurations that aren't secure.
      - * Follow the :ref:`TF-A<TF-A threat model>`, :ref:`OP-TEE<OP-TEE threat model>`, :ref:`TPM (fTPM)<TPM threat model>` recommended configurations for building a secure product.
        * Follow recommendations telling how to configure OCI-based containers for security oriented end products.

    * - **Physical access to configuration**
      - The device can be deployed in a location where people have physical
        access to the device, which also means that they might try to change
        configurations.
      - * Boot time integrity checking of configurations.
        * Run-time integrity checking of configurations using for example `IMA`_ (`D3-FH`_).

    * - **Bootloader Authentication**
      - A legitimate user could try to replace or modify the firmware binaries.
      - * Signature verification using RSA or ECDSA. (`D3-BA`_, `D3-FV`_)
        * Measured boot (`D3-TBI`_)

    * - **Corrupting memory**
      - An attacker can try to modify memory to gain control of the execution
        (ROP, JOP attacks etc).
      - * Pointer Authentication (`PAC`_) - requires Arm v8.3A. (`D3-PAN`_)
        * Branch Target Identification (`BTI`_) - requires Arm v8.5A.
        * Memory Tagging Extension (`MTE`_) - requires Arm v8.5A.
        * Stack Frame Canary Validation (`D3-SFCV`_) using for example GCC and
          ``-fstack-protector``.
        * ASLR (`D3-SAOR`_) to randomize base addresses.

    * - **Disk modification**
      - An attacker physically move a disk or boot the machine in another OS and
        then try to alter the content on the disk.
      - * Disk Encryption (`D3-DENCR`_).

    * - **Containers accessing host resources**
      - Containers can run with elevated privleges, which can affect the
        security of the system.
      - * Avoid using ``--privileged``, but at least document when using it and
          state why it is needed and what potential risks are.
        * Enable Mandatory Access Control (MAC) in form of Seccomp, SELinux etc.
        * Leverage ``cgroups`` to limit the access to system resources.

    * - **Container modification**
      - An attack can try to replace or modify the container.
      - * `Sign and verify containers <https://github.com/containers/podman/blob/main/docs/tutorials/image_signing.md>`_ (also see ``podman image trust``, ``podman image sign``).
        * Measure containers - Leverage TPM or fTPM to measure the containers
          (extend PCRs).

    * - **Security vulnerabilities present in the container image**
      - The container image may contain binaries that might have known
        vulnerabilities and could be use as an exploit.
      - * Regularly update the binaries in the container image, so the binaries in
          the container image always is up-to-date.
        * Run scanners in the container image to find vulnerable binaries.

    * - **PKI key replacement**
      - An attacker could try to change the public keys used to verify signed
        images.
      - * Make public keys immutable.
        * Leverage chain-of-trust all the way from the boot ROM.
        * Use certificates to assure the owner and origin of a public key.

    * - **Changed PCR values**
      - An attacker could try to update PCR values at random points all the way
        from the boot and when the system is fully up and running. A succesful
        attack like this would work as a denial of service attack, since it
        wouldn't be possible to make a successful remote attestation.
      - * Give only administers permission to update PCRs.
        * Enable Mandatory Access Control (MAC) in form of Seccomp, SELinux etc.
        * Leverage ``cgroups`` to limit the access to system resources.

    * - **Network access**
      - Open ports and unnecessary services may expose an attack surface.
      - * Leverage firewalls.
        * Disable unused services.

    * - **Leverage debug capabilities**
      - On a production system there is probably no need to have debugging
        capabilities enabled by default.
      - Disable ``ptrace`` (`Fedora example <https://docs.fedoraproject.org/en-US/Fedora/21/html/SELinux_Users_and_Administrators_Guide/sect-Security-Enhanced_Linux-Working_with_SELinux-Disable_ptrace.html>`_) or use other ways to disable the possibility to attach to other
        processes.

    * - **Multitenancy environment**
      - A legitimate administrator could get access to the containers and the
        content running inside the container.
      - This is the case where we need confidential computing support, something
        like Arm CCA. However, that technology is still under development, so
        until then we won't be able to do much about. Also, running things in
        Realms will be different compared to running in a container, so things
        are not directly comparable.

    * - **Establish a reverse tunnel**
      - An exploit giving a shell could give an attacker the opportunity to
        setup a reverse tunnel.
      - Prevent the initiation of outbound traffic.

    * - **DNS spoofing** (`D3-DNSTA`_)
      - When use cases consisting of devices communicating with each other there
        is a risk that someone spoof the DNS resolution. Argueable there are
        other mechnisms (signature verification on downloadables etc) that would
        prevent potential security implications by not protecting DNS
        resolution.
      - Introduce `DNSSEC`_ at devices exposing services to clients.


Other projects threat models
****************************

.. _TF-A threat model:

TF-A
=====

TrustedFirmware-A (TF-A) gives its analysis for threat model
(`ARM-TFA-THREAT-MODEL`_) and provides insecure configurations to
mitigate potential threat.  TRS suggests to enable these insecure
configurations for a production ready build, the relevant flags are
listed as below.

.. list-table:: TrustedFirmware-A (TF-A) insecure configurations
    :widths: 50 50
    :header-rows: 1
    :align: left

    * - Inscure configurations
      - Description

    * - ENABLE_STACK_PROTECTOR=strong
      - Enable the stack protection checks in GCC, the stack protection
        level "strong" is suggested.

    * - BRANCH_PROTECTION=1
      - Enable the branch protection feature, setting to 1 means
        "Enables all types of branch protection features", it requires
        ARMv8.3 Pointer Authentication and ARMv8.5 Branch Target
        Identification are supported.  Otherwise, if the CPUs on your
        platform cannot support one or both of these two CPU features,
        you need to select other values or event disable branch
        protection with setting value to 0.  The detailed information
        can be found in the document `ARM-TFA-BUILD-OPTIONS`_.
        To be able to leverage and build this feature, two additional
        flags needs to be enabled: CTX_INCLUDE_PAUTH_REGS=1 and
        ARM_ARCH_MINOR, we must pick the value for ARM_ARCH_MINOR based
        on the CPU architecture version, e.g. when validate on QEMU
        aarch64 with support Armv8.5 architecture, we set
        ARM_ARCH_MINOR=5 for this case.

    * - DECRYPTION_SUPPORT=aes_gcm
      - Select the authenticated decryption algorithm for firmware.

    * - ENCRYPT_BL31=1
      - Enable encryption for BL31 firmware.

    * - ENCRYPT_BL32=1
      - Enable encryption for BL32 firmware.

    * - KEY_ALG=rsa / KEY_SIZE=4096
      - Select the RSA algorithm for the PKCS keys and signing keys
        and the key size is 4096.  When the large key size (4096) is
        used instead of the default key size of 2048, the product is
        better protected.

    * - MEASURED_BOOT=1 / EVENT_LOG_LEVEL=10 / TPM_HASH_ALG
      - Enables measured boot option MEASURED_BOOT=1 when a platform
        supports TPM, we can emulate TPM with the tool `swtpm` on QEMU
        platform, the details for enabling TPM on QEMU can be found
        in the document `QEMU-TPM`_.  Setting EVENT_LOG_LEVEL=10 for
        only printing out TPM error log.  TPM are used not only by TF-A
        but also by bootloaders and operating systems, usually the TPM
        PCR bank algorithm is chosen by later bootloader, this is reason
        why TF-A needs to explicitly specify TPM hash algorithm (e.g.
        set TPM_HASH_ALG=sha256) which is chosed by later bootloader and
        avoid incompatible issue between them.

    * - DRTM_SUPPORT=1
      - Enable Dynamic Root of Trust for Measurement (DRTM).

As a reference, the TF-A recipe (`QEMU-AARCH64-RECIPE`_) will enable
above insecure configurations for building booting images for QEMU
aarch64.

.. _OP-TEE threat model:

OP-TEE
======

Invoking the TEE from a container
---------------------------------

Containers can access the services provided by OP-TEE as long as:

 * The OP-TEE client libraries (```optee-client``` package) are installed in
   the container
 * The ``/dev/tee0`` device is exposed to the container. With Docker, this is
   achieved via ``--device /dev/tee0``.
   For example:

   .. code-block:: bash

      $ docker run -it --device /dev/tee0 <docker-image>

With such a configuration, only the client side is deployed in the container;
all the other components of the TEE are on the host. This includes:

  * The OP-TEE kernel driver
  * The MMC RPMB kernel driver (when OP-TEE's ```CFG_RPMB_FS``` is enabled)
  * The ``tee-supplicant`` process
  * The files created in the host's root filesystem by ``tee-supplicant`` to
    provide storage for TEE persistent objects (when OP-TEE's ``CFG_REE_FS``
    is enabled)
  * The OP-TEE OS
  * The Trusted Applications binaries (```*.ta``` files)

More complex configurations are possible, for example:
  * Running ``tee-supplicant`` in a container. For this ``dev/teepriv0``
    has to be shared with the container via ``--device /dev/teepriv0``. Only one
    instance of the supplicant process may be running at any given time, so the
    host instance has to be stopped before the container is started.
  * Loading Trusted Application from a container or moving secure storage into
    a container. ``tee-supplicant`` loads TAs from ``/lib/optee_armtz`` and
    manages data files for secure storage in ``/data/tee`` by default. Therefore,
    Docker bind mounts as well as host overlay mounts may be used to compose
    things in a creative way.


.. _U-Boot threat model:

U-Boot
======

Unlike TF-A, U-Boot doesn't give any offical documentation for handling
potential threats.  Below lists insecure configurations which are
suggested by TRS for a production ready build.

.. list-table:: U-Boot insecure configurations
    :widths: 50 50
    :header-rows: 1
    :align: left

    * - Insecure configurations
      - Description

    * - CONFIG_TPM / CONFIG_EFI_TCG2_PROTOCOL / CONFIG_EFI_TCG2_PROTOCOL_EVENTLOG_SIZE
      - Support TPM device on the platform, and enabling EFI_TCG2
        configurations to produce EventLog with the TPM.

    * - CONFIG_TEE / CONFIG_RNG_OPTEE
      - Enable driver for OP-TEE and create connection with secure
        world's OP-TEE firmware.  Enable the OP-TEE based Random
        Number Generator.

    * - CONFIG_EFI_RUNTIME_UPDATE_CAPSULE / CONFIG_EFI_CAPSULE_FIRMWARE / CONFIG_EFI_CAPSULE_FIRMWARE_RAW / CONFIG_EFI_CAPSULE_FIRMWARE_FIT
      - With these configurations, we can update the U-Boot image using
        the UEFI firmware management protocol (fmp).  Enable
        CONFIG_EFI_CAPSULE_FIRMWARE_FIT to support FIP image with using
        the same protocol.

    * - CONFIG_CMD_EFICONFIG / CONFIG_CMD_BOOTMENU / CONFIG_AUTOBOOT_MENU_SHOW / CONFIG_BOOTMENU_DISABLE_UBOOT_CONSOLE
      - Enable the first three configurations CONFIG_CMD_EFICONFIG and
        CONFIG_CMD_BOOTMENU, U-Boot supports UEFI menu interface.  After
        enabled the configuration CONFIG_AUTOBOOT_MENU_SHOW, UEFI menu
        can be shown up automatically.  To only disply UEFI menu and
        disable U-Boot console, we can enable the configuration
        CONFIG_BOOTMENU_DISABLE_UBOOT_CONSOLE, in this case, we also
        need to remove configuration CONFIG_PREBOOT so can avoid adding
        boot manager entry in UEFI menu.  Please see the details in the
        document `UBOOT-EFICONFIG`_.

    * - CONFIG_SILENT_CONSOLE
      - With configuration CONFIG_SILENT_CONSOLE and append "silent=1"
        into the U-Boot environment (e.g. append it into the macro
        CONFIG_EXTRA_ENV_SETTINGS), we can totally mute console for
        U-Boot.


.. _TPM threat model:

TPM
===

..
  [NEEDS_TO_BE_FIXED] - Content to be added.

.. _fTPM threat model:


Firmware TPM
============

..
  [NEEDS_TO_BE_FIXED] - Content to be added.


OCI
===

..
  [NEEDS_TO_BE_FIXED] - Content to be added.


Links
*****

* https://nvlpubs.nist.gov/nistpubs/ir/2022/NIST.IR.8320B.pdf

.. _BTI: https://developer.arm.com/documentation/ddi0596/2021-06/Base-Instructions/BTI--Branch-Target-Identification
.. _DNSSEC: https://www.icann.org/resources/pages/dnssec-what-is-it-why-important-2019-03-05-en
.. _MITRE D3FEND threat model matrix: https://d3fend.mitre.org
.. _D3-BA: https://d3fend.mitre.org/technique/d3f:BootloaderAuthentication
.. _D3-ACH: https://d3fend.mitre.org/technique/d3f:ApplicationConfigurationHardening
.. _D3-PAN: https://d3fend.mitre.org/technique/d3f:PointerAuthentication
.. _D3-SFCV: https://d3fend.mitre.org/technique/d3f:StackFrameCanaryValidation
.. _D3-SAOR: https://d3fend.mitre.org/technique/d3f:SegmentAddressOffsetRandomization
.. _D3-DENCR: https://d3fend.mitre.org/technique/d3f:DiskEncryption
.. _D3-TBI: https://d3fend.mitre.org/technique/d3f:TPMBootIntegrity
.. _D3-FH: https://d3fend.mitre.org/technique/d3f:FileHashing
.. _D3-FV: https://d3fend.mitre.org/technique/d3f:FirmwareVerification
.. _D3-DNSTA: https://d3fend.mitre.org/technique/d3f:DNSTrafficAnalysis
.. _IMA: https://sourceforge.net/p/linux-ima/wiki/Home/
.. _MTE: https://developer.arm.com/-/media/Arm%20Developer%20Community/PDF/Arm_Memory_Tagging_Extension_Whitepaper.pdf
.. _PAC: https://developer.arm.com/documentation/102433/0100/Return-oriented-programming
.. _podman sign: https://github.com/containers/podman/blob/main/docs/tutorials/image_signing.md
.. _ptrace: https://docs.fedoraproject.org/en-US/Fedora/21/html/SELinux_Users_and_Administrators_Guide/sect-Security-Enhanced_Linux-Working_with_SELinux-Disable_ptrace.html
.. _ARM-TFA-THREAT-MODEL: https://trustedfirmware-a.readthedocs.io/en/latest/threat_model/index.html
.. _ARM-TFA-BUILD-OPTIONS: https://trustedfirmware-a.readthedocs.io/en/latest/getting_started/build-options.html#common-build-options
.. _QEMU-TPM: https://qemu.readthedocs.io/en/latest/specs/tpm.html
.. _QEMU-AARCH64-RECIPE: https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/blob/master/meta-trustedsubstrate/recipes-bsp/trusted-firmware-a/trusted-firmware-a-tsqemuarm64-secureboot.inc
.. _UBOOT-EFICONFIG: https://u-boot.readthedocs.io/en/latest/usage/cmd/eficonfig.html?highlight=EFICONFIG
